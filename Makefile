CLAGS=-g
 
CC = g++
AUTHOR = xhynek09_xkonec63
FILES =

all: library

library: library.cpp
	$(CC) $(CFLAGS) -o $@ library.cpp

rebuild: clean all

run:
	./library -t 20
	
clean:
	rm -f library *.tar.gz *.zip *.out

tarball:
	tar -zcf $(AUTHOR).tar.gz $(FILES)

tar:
	tar -cf $(AUTHOR).tar $(FILES)

zip:
	zip 07_$(AUTHOR).zip $(FILES)