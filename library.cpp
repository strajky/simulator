/************************************************************************* 
*	Projekt: IMS zadání č.1 Diskrétní simulátor s kalendářem událostí
*	Autor01: xhynek09, Tomáš Hynek
*	Autor02: xkonec63, Adam Konečný
*	Ročnik: VUT FIT 3BIT
* 	Datum: 6.12.2015
**************************************************************************/
#include <stdio.h>
#include <time.h>
#include <math.h> 
#include <stdlib.h>
#include <iostream>
#include <list>
#include <random>

#include <cmath>

using namespace std;

time_t getCurrentTime() {
	time_t timeVal;
	time(&timeVal);

	return timeVal;
}

/* ******************** ENTITY ******************** */
class Entity {
   public:
      time_t entryTime;   	// time of entry
      int priority;  	  	// service priority
      int id;

      Entity(int _priority, int _id, time_t _entryTime);
};

Entity::Entity(int _priority, int _id, time_t _entryTime) {
	entryTime = _entryTime;
	priority = _priority;
	id = _id;
}

/* ******************** QUEUE ******************** */
class Queue {
	public:
		Queue();
		void pushEntity(Entity *entity);
		Entity *popFirstEntity();
	private:
		list<Entity *> *queue;
};

bool compare_time(const Entity::Entity *first, const Entity::Entity *second) {
	if (first->entryTime == second->entryTime) {
		if (first->priority < second->priority) {
			return true;
		} else {
			return (first->id < second->id);
		}
	} else {
  		return (first->entryTime < second->entryTime);
	}
}

Queue::Queue() {
	static list<Entity *> list;

	queue = &list;
}

void Queue::pushEntity(Entity *entity) {
	queue->push_back(entity);

	queue->sort(compare_time);
}

Entity *Queue::popFirstEntity() {
	Entity *entity = queue->front();

	if (entity) {
		queue->pop_front();

		return entity;
	} else {
		return NULL;
	}
}

/* ****************** FACILITY ****************** */
class Facility {
	public:
		bool isFree;
		Facility();
		void Seize(Entity *_entity);
		Entity *Release();

	private:
		Entity *entity;
};

Facility::Facility() {
	isFree = true;	
}

void Facility::Seize(Entity *_entity) {
	if (isFree) {
		entity = _entity;
	}

	isFree = false;
}

Entity *Facility::Release() {
	isFree = true;

	return entity;
}

/* ******************** STORE ******************** */
class Store {
	public:
		int quantity;
		Store(int _quantity);
		bool removeFromStore();
		void addToStore();
};

Store::Store(int _quantity) {
	quantity = _quantity;	
}

bool Store::removeFromStore() {
	
	if (quantity != 0) {
		quantity = quantity - 1;
		
		return true;
	} else {
		return false;
	}
}

void Store::addToStore() {
	quantity = quantity + 1;
}

/* ******************** CALENDAR EVENT ******************** */

class Event {
	public:
		Event(int _type, float _duration);
		int type;
		float duration;
		time_t start;
		int priority;
		int id;
};

Event::Event(int _type, float _duration) {
	type = _type;
	duration = _duration;

	id = 0;
	priority = 0;

	start = getCurrentTime();
}


/* ******************** CALENDAR ******************** */

class Calendar {
	public:
		Calendar();
		void insertEvent(Event *event);

	private:
		list<Event *> *queue;
		int counter;
};

Calendar::Calendar() {
	static list<Event *> list;
	queue = &list;
	counter = 0;
}

bool compare_time_event(const Event::Event *first, const Event::Event *second) {
	if (first->start == second->start) {
		if (first->priority < second->priority) {
			return true;
		} else {
			return (first->id < second->id);
		}
	} else {
  		return (first->start < second->start);
	}
}

void Calendar::insertEvent(Event *event) {
	event->id = counter;
	counter++;

	queue->push_back(event);

	queue->sort(compare_time_event);
}

double Exponential(double mv) {
	double exp = -mv * std::log(random());

	return exp;
}

/* ******************** MAIN ******************** */
int main(int argc, const char* argv[]) {
	char *err;
	int count = strtol(argv[2], &err, 10);

	/*
	cout << Exponential(0.02);

	int nrolls=10000;  // number of experiments

	default_random_engine generator;
	exponential_distribution<double> distribution(100.0);

	int p[20]={};

	double flem = 0.0;

  	for (int i=0; i<nrolls; ++i) {
    	double number = distribution(generator);

    	flem += number;

    	if (number < 1.0) {
    		++p[int(count * number)];
    	}
  	}

  	cout << "Average: " << flem / 20.0 <<  endl;

  	cout << "exponential_distribution (10.0):" << endl;
  	cout << std::fixed; std::cout.precision(1);

	for (int i = 0; i < count; ++i) {
		cout << p[i] << endl;
		//std::cout << float(i)/count << "-" << float(i+1)/count << ": ";
		//std::cout << std::string(p[i]*nstars/nrolls,'*') << std::endl;
	}
	*/

	Queue *queue = new Queue();
	Facility *facility = new Facility();

	for (int i = 0; i <= count; i++) {
		double timeNewEntity = 10 * i;//exponential_distribution(10.0);

		int priority = rand() % 10;
		priority = priority > 8 ? 0 : 1;

		Entity *entity = new Entity(priority, i, getCurrentTime() + timeNewEntity);
/*
		cout << "############ Vytvořena entita " << i << " ############" << "\n";
		cout << "# priorita: " << entity->priority << "\n";
		cout << "# vstupní čas: " << entity->entryTime << "\n";
*/
		queue->pushEntity(entity);
	}

	Entity *entity;

	while ((entity = queue->popFirstEntity())) {
		cout << "Jdu pryč. " << entity->id << " " << entity->priority << " " << entity->entryTime << "\n";
	}

   return 0;
}


//void Facility (const char name);
//void Seize (Entity *object, int priority);
//void Release (Entity *e);